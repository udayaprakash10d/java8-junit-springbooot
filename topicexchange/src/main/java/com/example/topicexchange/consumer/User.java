package com.example.topicexchange.consumer;

import com.example.topicexchange.config.MessageConfig;
import com.example.topicexchange.dto.OrderStatus;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class User {
    @RabbitListener(queues = MessageConfig.QUEUE1)
    public void consumeMessageFromQueue(OrderStatus orderStatus){
        System.out.println("message received from queue"+orderStatus);
    }

    @RabbitListener(queues = MessageConfig.QUEUE_ALL)
    public void consumeMessageFromFanoutQueue(OrderStatus orderStatus){
        System.out.println("message received from queue"+orderStatus);
    }
}
