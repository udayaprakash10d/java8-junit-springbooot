package com.example.topicexchange.publisher;

import com.example.topicexchange.config.MessageConfig;
import com.example.topicexchange.dto.Order;
import com.example.topicexchange.dto.OrderStatus;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/order")
public class OrderPublisher {
    @Autowired
    public RabbitTemplate template;

    @PostMapping("/{restaurantName}")
    public String bookOrder(@RequestBody Order order, @PathVariable String restaurantName)
    {
      order.setOrderId(UUID.randomUUID().toString());
        OrderStatus orderStatus=new OrderStatus(order,"process","order placed in"+restaurantName);
        template.convertAndSend(MessageConfig.TOPIC_EXCHANGE,MessageConfig.ROUTING_KEY1,orderStatus);
        return "success!!!!!!";
    }

    @PostMapping("/order1/{message}")
    public String bookOrder1(@RequestBody Order order, @PathVariable String message)
    {
        order.setOrderId(UUID.randomUUID().toString());
        MessageProperties messageProperties=new MessageProperties();
        messageProperties.setHeader("colour",order);
        MessageConverter messageConverter=new SimpleMessageConverter();
        Message order1=messageConverter.toMessage(message,messageProperties);
        template.send(MessageConfig.HEADER_EXCHANGE,"",order1);
        return "success!!!!!!Header";
    }
}
