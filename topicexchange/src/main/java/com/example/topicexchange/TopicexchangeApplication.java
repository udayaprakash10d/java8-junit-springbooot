package com.example.topicexchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopicexchangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopicexchangeApplication.class, args);
	}

}
