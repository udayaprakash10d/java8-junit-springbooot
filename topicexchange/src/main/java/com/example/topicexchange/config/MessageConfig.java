package com.example.topicexchange.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageConfig {

    public static final String QUEUE1 ="queue.A";
    public static final String QUEUE2 ="queue.B";
    public static final String QUEUE_ALL ="queue.all";
    public static final String QUEUE_H1 ="queue.H1";
    public static final String QUEUE_H2 ="queue.H2";
    public static final String TOPIC_EXCHANGE ="topic_exchange";
    public static final String HEADER_EXCHANGE ="header_exchange";
    public static final String ROUTING_KEY1 ="routing.A";
    public static final String ROUTING_KEY2 ="routing.B";
    public static final String ROUTING_KEY_ALL ="routing.*";

    @Bean
    public Queue queue1()
    {

        return new Queue(QUEUE1);
    }
    @Bean
    public Queue queueH1()
    {
        return new Queue(QUEUE_H1);
    }
    @Bean
    public Queue queueH2()
    {
        return new Queue(QUEUE_H2);
    }
    @Bean
    public Queue queue2()
    {
        return new Queue(QUEUE2);
    }
    @Bean
    public Queue queueAll()
    {
        return new Queue(QUEUE_ALL);
    }
    @Bean
    public TopicExchange exchange()
    {

        return new TopicExchange(TOPIC_EXCHANGE);
    }
    @Bean
    public HeadersExchange exchangeH()
    {

        return new HeadersExchange(HEADER_EXCHANGE);
    }
    @Bean
    public Binding bindingH1(Queue queueH1, HeadersExchange exchangeH)
    {
        return BindingBuilder.bind(queueH1).to(exchangeH).where("colour").matches("red");
    }
    @Bean
    public Binding bindingH2(Queue queueH2, HeadersExchange exchangeH)
    {
        return BindingBuilder.bind(queueH2).to(exchangeH).where("colour").matches("blue");
    }

    @Bean
    public Binding binding1(Queue queue1, TopicExchange exchange)
    {
        return BindingBuilder.bind(queue1).to(exchange).with(ROUTING_KEY1);
    }
    @Bean
    public Binding binding2(Queue queue2,TopicExchange exchange)
    {
        return BindingBuilder.bind(queue2).to(exchange).with(ROUTING_KEY2);
    }
    @Bean
    public Binding bindingAll(Queue queueAll,TopicExchange exchange)
    {
       return BindingBuilder.bind(queueAll).to(exchange).with(ROUTING_KEY_ALL);
    }

    @Bean
    public MessageConverter messageConverter()
    {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory)
    {
        final RabbitTemplate rabbitTemplate=new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return  rabbitTemplate;
    }
}
