//package com.learning.rabbitmq.consumer;
//
//import com.learning.rabbitmq.config.MessageConfig;
//import com.learning.rabbitmq.dto.OrderStatus;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
//@Component
//public class User {
//    @RabbitListener(queues = MessageConfig.QUEUE)
//    public void consumeMessageFromQueue(OrderStatus orderStatus){
//        System.out.println("message received from queue"+orderStatus);
//    }
//
//}
