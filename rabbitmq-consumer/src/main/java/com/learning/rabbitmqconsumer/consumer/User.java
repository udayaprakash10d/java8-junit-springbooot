package com.learning.rabbitmqconsumer.consumer;


import com.learning.rabbitmqconsumer.config.MessageConfig;
import com.learning.rabbitmqconsumer.dto.OrderStatus;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class User {
    @RabbitListener(queues = MessageConfig.QUEUE)
    public void consumeMessageFromQueue(OrderStatus orderStatus){
        System.out.println("message received from queue"+orderStatus);
    }

}
