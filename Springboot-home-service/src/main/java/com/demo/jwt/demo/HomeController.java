package com.demo.jwt.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api")
public class HomeController {
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping("/home")
	public String home()
	{
		//return "welcome home";
	String products=	restTemplate.getForObject("http://PRODUCT-MICROSERVICES/pro/products", String.class);
	return products;
	}

}
