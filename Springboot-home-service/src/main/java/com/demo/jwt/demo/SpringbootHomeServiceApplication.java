package com.demo.jwt.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringbootHomeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootHomeServiceApplication.class, args);
	}

}
