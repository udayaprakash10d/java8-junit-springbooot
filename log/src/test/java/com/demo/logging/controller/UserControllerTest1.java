package com.demo.logging.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.demo.logging.model.User;
import com.demo.logging.repo.UserRepo;
import com.demo.logging.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
//@WebMvcTest
@AutoConfigureMockMvc
public class UserControllerTest1 {
	@Autowired
	private WebApplicationContext context;

	@InjectMocks
	public UserController userController;
	@Mock
	public UserService userService;

	private MockMvc mvc;

	public UserControllerTest1() {
		userService = Mockito.mock(UserService.class);
		userController = new UserController(userService);
	}

	@MockBean
	private UserRepo userRepo;

//	@Before
//	public void setup()
//	{
//		System.out.println("setup method calling");
//		mvc=MockMvcBuilders.webAppContextSetup(context).build();
//	}

//	@Before
//	public void setup() {
//	mvc = MockMvcBuilders
//	.webAppContextSetup(context)
//	.build();
//	}
	public String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

	@Before
	public void setUp() {
		System.out.println("setup calling");
		this.mvc = MockMvcBuilders.standaloneSetup(userController).build();
	}

	@Test
	public void getUserByIdTest() throws Exception {

		this.mvc = MockMvcBuilders.standaloneSetup(userController).build();

		User mockUser1 = new User(1, "ud", 24);
		Mockito.doReturn(Optional.of(mockUser1)).when(userService).getUserById(1);

		mvc.perform(MockMvcRequestBuilders.get("/user/1"))
				.andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value()));

		// .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)));
		// .andExpect(MockMvcResultMatchers.content().string(containsString("ud")));
		Mockito.verify(userService).getUserById(1);
	}

	@Test
	public void getUserTest() throws Exception {
		this.mvc = MockMvcBuilders.standaloneSetup(userController).build();

		User user = new User(1, "abc", 18);
		// Mockito.when(userRepo.findById(1)).thenReturn(user);

		Mockito.when(userService.getUser()).thenReturn(Stream.of(new User(1, "aaa", 18)).collect(Collectors.toList()));
		mvc.perform(get("/users").accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());

//		                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
//		                .andExpect(MockMvcResultMatchers.jsonPath("$.name",Matchers.is("abc")))
//		                .andExpect(MockMvcResultMatchers.jsonPath("$.age",Matchers.is(18)));
		Mockito.verify(userService).getUser();
	}

	@Test
	public void postUserTest() throws Exception {
		this.mvc = MockMvcBuilders.standaloneSetup(userController).build();

		User user = new User(1, "abd", 8);
		String inputJson = mapToJson(user);
		mvc.perform(MockMvcRequestBuilders.post("/user").contentType(MediaType.APPLICATION_JSON).content(inputJson))
				.andExpect(MockMvcResultMatchers.status().isOk());
//		                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
//		                .andExpect(MockMvcResultMatchers.jsonPath("$.name",Matchers.is("abd")))
//		                .andExpect(MockMvcResultMatchers.jsonPath("$.age",Matchers.is(8)));
	}

	@Test
	public void deleteUserTest() throws Exception {
		this.mvc = MockMvcBuilders.standaloneSetup(userController).build();
		User user = new User(1, "abc", 18);
		mvc.perform(MockMvcRequestBuilders.delete("/user/1").accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

}
