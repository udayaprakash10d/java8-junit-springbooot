package com.demo.logging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.demo.logging.model.User;
import com.demo.logging.repo.UserRepo;
import com.demo.logging.service.UserService;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class LogApplicationTests {

	MockMvc mvc;

	@Autowired
	UserService userService;

	@MockBean
	UserRepo userRepo;

	@Test
	public void getuserTest()

	{
		when(userRepo.findAll())
				.thenReturn(Stream.of(new User(1, "aaa", 18), new User(2, "bbb", 18)).collect(Collectors.toList()));
		assertEquals(2, userService.getUser().size());
		verify(userRepo).findAll();
		verify(userRepo, times(1)).findAll();

	}

	@Test
	public void postUserTest() {
		User user = new User(1, "zzz", 10);
		when(userRepo.save(user)).thenReturn(user);
		assertEquals(user, userService.postUser(user));
		verify(userRepo, times(1)).save(user);

	}

	@Test
	public void DeleteUserTest() {
		User user = new User(1, "zzz", 10);
		userService.deleteUserById(1);
		verify(userRepo, times(1)).deleteById(1);
	}
//	@Test
//	public void getUserApiTest() throws Exception 
//	{
//	  mvc.perform( MockMvcRequestBuilders
//	      .get("/users")
//	      .accept(MediaType.APPLICATION_JSON))
//	      .andDo(print())
//	      .andExpect(status().isOk())
//	      .andExpect(MockMvcResultMatchers.jsonPath("$.users").exists())
//	      .andExpect(MockMvcResultMatchers.jsonPath("$.users[*].employeeId").isNotEmpty());
//	}

//	@Test
//	public void getUserByIdTest()
//	{
//		int id=1;
//		when(userRepo.findById(id)).thenReturn(Stream.of(new User(1,"aaa",1)).collect(Collectors.toList()))
//		assertEquals(1,userService.getUserById(id).);
//	}

}
