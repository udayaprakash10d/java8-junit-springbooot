package com.demo.logging.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.logging.model.User;
import com.demo.logging.repo.UserRepo;

@Component
public class UserService {
	@Autowired
	UserRepo userRepo;

	public User postUser(User user) {

		return userRepo.save(user);
	}

	public List<User> getUser() {
		List<User> user = userRepo.findAll();
		System.out.println("getting data from db" + user);
		return user;
	}

	public Optional<User> getUserById(int id) {

		return userRepo.findById(id);
	}

	public void deleteUserById(int id) {
		userRepo.deleteById(id);

	}

}
