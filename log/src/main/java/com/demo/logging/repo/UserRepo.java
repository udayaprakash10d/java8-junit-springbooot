package com.demo.logging.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.demo.logging.model.User;


@Repository
public interface UserRepo extends JpaRepository<User, Integer> {

	

}
