package com.demo.logging.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.logging.model.User;
import com.demo.logging.service.UserService;

@RestController
public class UserController {

	Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/users")
	public List<User> getUser() {

		log.info("getUser method called");
		System.out.println("get user method called");
		return userService.getUser();

	}

	@GetMapping("/user/{id}")
	public Optional<User> getUserById(@PathVariable("id") int id) {
		log.info("getUser by id method called");
		System.out.println("getUser by id method called");
		return userService.getUserById(id);

	}

	@PostMapping("/user")
	public User postUser(@RequestBody User user) {
		System.out.println("postuser method called");
		return userService.postUser(user);
	}

	@DeleteMapping("/user/{id}")
	public void deleteUserById(@PathVariable("id") int id) {
		System.out.println("delete user by id called");
		userService.deleteUserById(id);
	}

}
