package com.example.directexchange.publisher;

import com.example.directexchange.config.MessageConfig;
import com.example.directexchange.dto.Order;
import com.example.directexchange.dto.OrderStatus;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/order")
public class OrderPublisher {
    @Autowired
    public RabbitTemplate template;

    @PostMapping("/{restaurantName}")
    public String bookOrder(@RequestBody Order order, @PathVariable String restaurantName) {
        order.setOrderId(UUID.randomUUID().toString());
        OrderStatus orderStatus = new OrderStatus(order, "process", "order placed in" + restaurantName);
        template.convertAndSend(MessageConfig.DIRECT_EXCHANGE, MessageConfig.ROUTING_KEY1, orderStatus);
        return "success!!!!!!";
    }
}
