package com.example.directexchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DirectexchangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DirectexchangeApplication.class, args);
	}

}
