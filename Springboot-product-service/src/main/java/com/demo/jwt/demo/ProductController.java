package com.demo.jwt.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pro")
public class ProductController {
	
	@Value("${server.port}")
	private int port;
	
	@GetMapping("/products")
	public String product()
	{
		return "products" +port ;
	}

}
