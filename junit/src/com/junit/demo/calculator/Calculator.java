package com.junit.demo.calculator;

public class Calculator {

	public static int add(int number1, int number2)
	{
		 int add= number1+number2;
		 System.out.println("add result----->" +add);
		 return add;
	}
	
	public static int sub(int number1, int number2)
	{
		int sub= number1-number2;
		System.out.println("sub result----->" +sub);
		return sub;
	}
	
	public static int mul(int number1, int number2)
	{
		int mul= number1*number2;
		System.out.println("mul result----->" +mul);
		return mul;
	}
	public static int div(int number1, int number2)
	{
		if(number2==0)
		{
			throw new IllegalArgumentException("number can't divide by 0");
		}
		int div =number1/number2;
		System.out.println("div result----->" +div);
		return div;
	}
}
