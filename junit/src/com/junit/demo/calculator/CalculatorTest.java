package com.junit.demo.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;;

public class CalculatorTest {
	@BeforeClass
	public static void preClass() {
		System.out.println("This is the preClass() method that runs one time before the class");
	}

	@Before
	public void setup() {
		System.out.println("before test setup method called");

	}

	@Test
	public void addTest() {
		assertEquals(10, Calculator.add(5, 5));
		assertNotEquals(10, Calculator.add(5, 1));
	}

	@Test
	public void subTest() {
		assertEquals(0, Calculator.sub(5, 5));
		assertNotEquals(10, Calculator.sub(5, 1));
	}

	@Test
	public void mulTest() {
		assertEquals(50, Calculator.mul(10, 5));
		assertNotEquals(10, Calculator.mul(5, 1));
	}

	@Test
	public void divTest() {
		assertEquals(2, Calculator.div(10, 5));
		assertNotEquals(10, Calculator.div(5, 1));
	}
	@After
	public void abc()
	{
		System.out.println("after test abc method called");
	}

	@AfterClass
	public static void postClass() {
		System.out.println("This is the postClass() method that runs one time after the class");
	}

}
